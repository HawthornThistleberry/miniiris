VERSION 4.00
Begin VB.Form frmMiniIRIS 
   Caption         =   "MiniIRIS"
   ClientHeight    =   1965
   ClientLeft      =   1740
   ClientTop       =   1590
   ClientWidth     =   3030
   Height          =   2370
   Icon            =   "MiniIRIS.frx":0000
   Left            =   1680
   LinkTopic       =   "Form1"
   ScaleHeight     =   1965
   ScaleWidth      =   3030
   Top             =   1245
   Width           =   3150
   Begin VB.CommandButton cmdDelete 
      Caption         =   "Delete"
      Enabled         =   0   'False
      Height          =   375
      Left            =   0
      TabIndex        =   9
      Top             =   1560
      Width           =   1455
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "Add"
      Height          =   375
      Left            =   0
      TabIndex        =   8
      Top             =   1080
      Width           =   1455
   End
   Begin VB.TextBox txtStatus 
      BackColor       =   &H00C0C0C0&
      Height          =   285
      Left            =   600
      Locked          =   -1  'True
      TabIndex        =   7
      Top             =   720
      Width           =   2415
   End
   Begin VB.TextBox txtName 
      BackColor       =   &H00C0C0C0&
      Height          =   285
      Left            =   600
      Locked          =   -1  'True
      TabIndex        =   6
      Top             =   360
      Width           =   2415
   End
   Begin VB.TextBox txtPhase 
      BackColor       =   &H00C0C0C0&
      Height          =   285
      Left            =   600
      Locked          =   -1  'True
      TabIndex        =   5
      Text            =   "0"
      Top             =   0
      Width           =   2415
   End
   Begin VB.CommandButton cmdQuit 
      Cancel          =   -1  'True
      Caption         =   "Quit"
      Height          =   375
      Left            =   1560
      TabIndex        =   4
      Top             =   1560
      Width           =   1455
   End
   Begin VB.CommandButton cmdTakeAction 
      Caption         =   "Take Action"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   375
      Left            =   1560
      TabIndex        =   3
      Top             =   1080
      Width           =   1455
   End
   Begin VB.Label lblStatus 
      Caption         =   "Status"
      Height          =   255
      Left            =   0
      TabIndex        =   2
      Top             =   750
      Width           =   495
   End
   Begin VB.Label lblPhase 
      Caption         =   "Phase"
      Height          =   255
      Left            =   0
      TabIndex        =   1
      Top             =   30
      Width           =   495
   End
   Begin VB.Label lblName 
      Caption         =   "Name"
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   390
      Width           =   495
   End
End
Attribute VB_Name = "frmMiniIRIS"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Dim Battle As New clsCombatant

' This function is called whenever anything updates the list of
' combatants, to make sure the first available action is displayed
' and the appropriate buttons are enabled.
Private Sub GetNextAction()
  txtName = Battle.FindNextAction
  If txtName = "" Then ' there must be no combatants
      txtPhase = "0"
      txtStatus = ""
      ' don't allow Take Action or Delete buttons
      cmdTakeAction.Enabled = False
      cmdDelete.Enabled = False
      Exit Sub
    End If
  ' update the display
  txtPhase = Trim$(Str$(Battle.NextAction(txtName)))
  txtStatus = ""
  If Battle.Stuns(txtName) > 0 Then txtStatus = "stunned"
  ' enable the TakeAction and Delete buttons
  cmdTakeAction.Enabled = True
  cmdDelete.Enabled = True
End Sub

Private Sub cmdAdd_Click()
  ' set the default next action phase
  frmAddCombatant.txtNextAction = Val(txtPhase) + 1
  ' launch the other form as a modal dialog
  frmAddCombatant.Show 1
  ' was it cancelled?
  If frmAddCombatant.cmdCancel.Tag <> "" Then Exit Sub
  ' create the new combatant
  If Battle.Create(frmAddCombatant.txtName, frmAddCombatant.txtDelay, _
                          frmAddCombatant.txtNextAction, frmAddCombatant.txtStuns) Then
      GetNextAction
    Else
      MsgBox "Unable to add combatant -- file full, or duplicate name.", 0, "MiniIRIS"
    End If
End Sub

Private Sub cmdDelete_Click()
  If Battle.Delete(txtName) Then
      GetNextAction
    Else
      MsgBox "Unable to delete combatant", 0, "MiniIRIS"
    End If
End Sub

Private Sub cmdQuit_Click()
  Unload Me
End Sub

Private Sub cmdTakeAction_Click()
  Dim WS As Integer
  ' set the weapon speed to whatever it was last
  WS = Battle.LastWS(txtName)
  ' get the weapon speed
  WS = InputBox("Weapon speed", "MiniIRIS", WS)
  ' now take the action
  Battle.TakeAction txtName, WS
  GetNextAction
End Sub

Private Sub Form_Load()
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
  txtPhase = "0"
End Sub

Private Sub Form_Unload(Cancel As Integer)
  End
End Sub
