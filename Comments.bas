Attribute VB_Name = "modComments"
Option Explicit

' This psuedo-module contains nothing but comments.

' The purpose of this program is to help a GameMaster of a roleplaying game
' called Prism to administer a battle.  This is a very simplified, miniaturized
' version of the initiative system for the sake of developing the object-
' oriented techniques, and can be built up into a full-fledged program.

' First, a little background.  One part of a roleplaying game is a simulation of
' a battle between a bunch of combatants, such as might appear in an action
' movie or book.  To make sure that everyone gets a fair chance at the fight
' in accord with their abilities, there are a large set of rules that are used to
' adjudicate the results of various combatant actions like attacks.  The
' backbone of all of this is what's called an "initiative system", a set of
' rules which determine who gets to act when, how often, and in what
' sequence.

' The way this particular initiative system (named IRIS) works is through a
' series of "phases", starting at 1 and counting up from there.  Each
' combatant has a "next action" phase.  We count phases upwards until
' someone's action comes up; then they get to do something, like run or
' attack -- in other words, take their action.  At that point, we figure out
' their new next action by adding a few factors together: a "Delay", which
' tells how fast they are; a "weapon speed" which modifies the delay to
' reflect how fast the weapon or other action they're taking is; and a random
' number from 1 to 10.  Adding these to the phase of the last action gives
' the phase of the next action.

' If someone is wounded in battle, one consequence is a "stun", which means
' that one or more of their upcoming actions will be spent being stunned.
' (While stunned, they can do some things but not others.)

' This program automates the process by keeping track of combatants and
' when their next actions are.  It tracks, for each combatant, the number of
' stuns they have, what their delay is, and what the last weapon speed they
' used was.  It presents what the next combatant action is and tells you if
' that combatant is stunned; then you can click a button to take their
' action, and it updates their next action phase, asking you for their weapon
' speed (defaulting to whatever they used last) and decrementing stuns if
' necessary.  Then it finds the *next* new action.

' In order to implement all of this, a class module named clsCombatant was
' created.  This object represents a list of combatants and all the information
' about them that is needed; and it provides a set of methods enabling the
' calling program to make selected inquiries of the database (only through
' these functions, providing encapsulation) and other methods to do all the
' operations and searches required.  This includes the high-level actions of
' taking an action (and calculating the next action), and finding the next
' combatant's next action.  For simplicity this program uses only this one
' class.  However, in a more full-fledged implementation, the program might
' use a class for a battle, which made use of a class for combatants which
' might include subclasses for different types of combatants (for instance,
' one detailed type for main characters about whom we know a lot, and
' another type with only the vital stats for "goons").

' All functions and declarations and code related to this class is bound up
' in the class module clsCombatant.  This program also includes two form
' files (and their associated code) and this comments module.  The first
' form is the main interface; the second one is used for user entry of the
' information for creating a new combatant.

' In the full-fledged version of this program (which I actually plan on working
' on this summer) the combatant record will include a lot more detail and the
' program will automate many more things such as different kinds of actions
' (movement, saving actions for later, etc.) and more kinds of wound effects
' (such as bleeding and initiative penalties).  The user interface will also
' include methods of editing combatants, saving and loading files of
' combatants and whole battles, etc.

