VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsCombatant"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

' This file contains the properties, methods, and declarations for
' the Combatant class.  This class represents the information we
' need to know about the combatants in a battle initiative.

' Note: this is the miniature version simplified for this project.
' The actual class would have much more information about the
' combatants, means of editing it, save and load methods,
' subclasses for different types of combatants, etc.

Const MAXCOMBATANTS = 50 ' fixed length array for simplicity

Private Type Combatant   ' stores all the information on one combatant
  Name As String              ' combatant name, must be unique
  Delay As Integer            ' base delay from one action to the next
  NextAction As Integer    ' current next action phase
  LastWS As Integer         ' weapon speed most recently used
  Stuns As Integer            ' number of actions of stun
End Type

' The properties of this class, all private:
Dim Combatants(MAXCOMBATANTS) As Combatant
Dim NumCombatants As Integer

' Initialization of the data when a new object of the class is instantiated
Private Sub Class_Initialize()
  NumCombatants = 0
End Sub

' Internal functions used by other functions within the class
Private Function FindByName(Name As String) As Integer
  Dim i As Integer
  i = 1
  Do While i <= NumCombatants And Combatants(i).Name <> Name
    i = i + 1
    Loop
  If i > NumCombatants Then
      FindByName = 0
      Exit Function
    End If
  FindByName = i
End Function

' Functions used to get the information about the combatants.  The calling
' routines should use these instead of accessing the Combatants() array
' directly.  Visual Basic doesn't actually forbid it, and you can't set the
' properties as private, so we have to take it on trust.

Function Delay(Name As String) As Integer
  Dim i As Integer
  i = FindByName(Name)
  If i = 0 Then
      Delay = 0
      Exit Function
    End If
  Delay = Combatants(i).Delay
End Function

Function LastWS(Name As String) As Integer
  Dim i As Integer
  i = FindByName(Name)
  If i = 0 Then
      LastWS = 0
      Exit Function
    End If
  LastWS = Combatants(i).LastWS
End Function

Function NextAction(Name As String) As Integer
  Dim i As Integer
  i = FindByName(Name)
  If i = 0 Then
      NextAction = 0
      Exit Function
    End If
  NextAction = Combatants(i).NextAction
End Function

Function Stuns(Name As String) As Integer
  Dim i As Integer
  i = FindByName(Name)
  If i = 0 Then
      Stuns = 0
      Exit Function
    End If
  Stuns = Combatants(i).Stuns
End Function

' Maintenance functions used to build the array
Function Create(Name As String, Delay As Integer, NextAction As Integer, _
                     Stuns As Integer) As Boolean
  If NumCombatants = MAXCOMBATANTS Or FindByName(Name) <> 0 Then
      Create = False
      Exit Function
    End If
  NumCombatants = NumCombatants + 1
  Combatants(NumCombatants).Name = Name
  Combatants(NumCombatants).Delay = Delay
  Combatants(NumCombatants).NextAction = NextAction
  Combatants(NumCombatants).LastWS = 0
  Combatants(NumCombatants).Stuns = Stuns
  Create = True
End Function

Function Delete(Name As String) As Boolean
  Dim i, j As Integer
  i = FindByName(Name)
  If i = 0 Then
      Delete = False
      Exit Function
    End If
  If i < NumCombatants Then
      For j = i To NumCombatants - 1
        Combatants(j).Name = Combatants(j + 1).Name
        Combatants(j).Delay = Combatants(j + 1).Delay
        Combatants(j).NextAction = Combatants(j + 1).NextAction
        Combatants(j).LastWS = Combatants(j + 1).LastWS
        Combatants(j).Stuns = Combatants(j + 1).Stuns
        Next j
    End If
  NumCombatants = NumCombatants - 1
  Delete = True
End Function

' The remaining functions are the backbone of this class as they manipulate
' combatant actions on a higher level of abstraction.  First, TakeAction
' is invoked when a combatant's action is taken, to set up all their values for
' the next one.  It calculates the next action, decrements stuns, and stores
' the last used weapon speed for easy re-use later.

Sub TakeAction(Name As String, WeaponSpeed As Integer)
  Dim i As Integer
  i = FindByName(Name)
  If i = 0 Then Exit Sub
  Combatants(i).LastWS = WeaponSpeed
  If Combatants(i).Stuns > 0 Then Combatants(i).Stuns = Combatants(i).Stuns - 1
  Combatants(i).NextAction = Combatants(i).NextAction + WeaponSpeed + _
      Combatants(i).Delay + Int(10 * Rnd + 1)
End Sub

' Finally, FindNextAction finds the combatant with the lowest value in the
' NextAction field and returns the name.  The main loop of the main program
' would be a series of FindNextAction calls, displaying the name of the
' resulting combatant (and using the property query functions to find and
' display other facts as needed), then TakeAction calls to move that
' combatant's action ahead.

Function FindNextAction() As String
  Dim i, lowest As Integer
  If NumCombatants = 0 Then
      FindNextAction = ""
      Exit Function
    End If
  lowest = 1
  For i = 2 To NumCombatants
    If Combatants(i).NextAction < Combatants(lowest).NextAction Then lowest = i
    Next i
  FindNextAction = Combatants(lowest).Name
End Function
