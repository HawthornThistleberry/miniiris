VERSION 4.00
Begin VB.Form frmAddCombatant 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Add Combatant"
   ClientHeight    =   1830
   ClientLeft      =   1755
   ClientTop       =   4050
   ClientWidth     =   3030
   Height          =   2235
   Icon            =   "frmAddCombatant.frx":0000
   Left            =   1695
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1830
   ScaleWidth      =   3030
   ShowInTaskbar   =   0   'False
   Top             =   3705
   Width           =   3150
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   1560
      TabIndex        =   9
      Top             =   1440
      Width           =   1455
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   375
      Left            =   0
      TabIndex        =   8
      Top             =   1440
      Width           =   1455
   End
   Begin VB.TextBox txtStuns 
      Height          =   285
      Left            =   960
      TabIndex        =   7
      Text            =   "0"
      Top             =   1080
      Width           =   2055
   End
   Begin VB.TextBox txtNextAction 
      Height          =   285
      Left            =   960
      TabIndex        =   6
      Text            =   "0"
      Top             =   720
      Width           =   2055
   End
   Begin VB.TextBox txtDelay 
      Height          =   285
      Left            =   960
      TabIndex        =   5
      Text            =   "30"
      Top             =   360
      Width           =   2055
   End
   Begin VB.TextBox txtName 
      Height          =   285
      Left            =   960
      TabIndex        =   4
      Top             =   0
      Width           =   2055
   End
   Begin VB.Label lblStuns 
      Alignment       =   1  'Right Justify
      Caption         =   "Stuns"
      Height          =   255
      Left            =   0
      TabIndex        =   3
      Top             =   1125
      Width           =   855
   End
   Begin VB.Label lblDelay 
      Alignment       =   1  'Right Justify
      Caption         =   "Delay"
      Height          =   255
      Left            =   0
      TabIndex        =   2
      Top             =   405
      Width           =   855
   End
   Begin VB.Label lblNextAction 
      Alignment       =   1  'Right Justify
      Caption         =   "Next Action"
      Height          =   255
      Left            =   0
      TabIndex        =   1
      Top             =   765
      Width           =   855
   End
   Begin VB.Label lblName 
      Alignment       =   1  'Right Justify
      Caption         =   "Name"
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   45
      Width           =   855
   End
End
Attribute VB_Name = "frmAddCombatant"
Attribute VB_Creatable = False
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()
  ' set the flag that the calling program knows it was cancelled
  cmdCancel.Tag = "y"
  Me.Hide
End Sub

Private Sub cmdOK_Click()
  txtName = Trim$(txtName)
  Me.Hide
End Sub

Private Sub Form_Activate()
  ' load and initialize the variables for a new entry
  txtName = ""
  txtDelay = "30"
  txtStuns = "0"
  cmdCancel.Tag = ""
  txtName.SetFocus
End Sub

Private Sub Form_Load()
  Left = (Screen.Width - Width) / 2
  Top = (Screen.Height - Height) / 2
End Sub

Private Sub txtDelay_GotFocus()
  txtDelay.SelStart = 0
  txtDelay.SelLength = Len(txtDelay)
End Sub

Private Sub txtDelay_KeyPress(KeyAscii As Integer)
  If (KeyAscii < Asc("0") Or KeyAscii > Asc("9")) And KeyAscii <> 8 Then
      KeyAscii = 0
      Beep
    End If
End Sub

Private Sub txtName_Change()
  If Trim$(txtName) = "" Then
      cmdOK.Enabled = False
    Else
      cmdOK.Enabled = True
    End If
End Sub

Private Sub txtName_GotFocus()
  txtName.SelStart = 0
  txtName.SelLength = Len(txtName)
End Sub

Private Sub txtNextAction_GotFocus()
  txtNextAction.SelStart = 0
  txtNextAction.SelLength = Len(txtNextAction)
End Sub

Private Sub txtNextAction_KeyPress(KeyAscii As Integer)
  If (KeyAscii < Asc("0") Or KeyAscii > Asc("9")) And KeyAscii <> 8 Then
      KeyAscii = 0
      Beep
    End If
End Sub

Private Sub txtStuns_GotFocus()
  txtStuns.SelStart = 0
  txtStuns.SelLength = Len(txtStuns)
End Sub

Private Sub txtStuns_KeyPress(KeyAscii As Integer)
  If (KeyAscii < Asc("0") Or KeyAscii > Asc("9")) And KeyAscii <> 8 Then
      KeyAscii = 0
      Beep
    End If
End Sub
